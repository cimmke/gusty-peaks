/****************************************************************************************
 * * Program Filename: substation.cpp
 * * Author: Corey Immke
 * * Date: 3/9/16
 * * Description: This is the implementation file for the substation class. This is 
 * *			  one of the spaces the player can be in.
 * * Input: There is no input other than using the special_action().
 * * Output: There is no output as this is the implementation file.
 ***************************************************************************************/
#include "substation.hpp"
#include "item.hpp"

#include <iostream>

/****************************************************************************************
 * * Function: substation()
 * * Description: This is the default constructor for the substation class. 
 * * Parameters: There are no parameters.
 * * Pre-Conditions: A substation object is created.
 * * Post-Conditions: The space_name and space_type fields are set.
 ***************************************************************************************/
substation::substation() : space("Grid", "Substation") {
}

/****************************************************************************************
 * * Function: special_action(technician*, game*)
 * * Description: This is the special_action() for the substation class. It allows the
 * *			  technician to switch the status of the grid from on to off or off to
 * *			  on. If the technician does not have the "High Voltage" safety gear
 * *			  in their bag they will lose the game.
 * * Parameters: A technician pointer for the technician object who is playing the game
 * *			 and a pointer to the game object to interact with the grid_status
 * *			 data element.
 * * Pre-Conditions: The user has chosen to use the special_action() function while the
 * *				 technician's location is the substation space.
 * * Post-Conditions: If the user chooses to, the grid_status element is switched. If
 * *				  the needed safety gear is not in the tech_bag the player loses.
 ***************************************************************************************/
void substation::special_action(technician *tech, game *g) {
		
	int choice;
	item *item_ptr;

	std::cout << "The current status of the grid is ";

	if (g->get_grid() == 0) {

		std::cout << "on." << std::endl;
		std::cout << std::endl;
	}

	else {

		std::cout << "off." << std::endl;
		std::cout << std::endl;
	}
	
	std::cout << "Would you like to switch the status of the grid?" << std::endl;
	std::cout << std::endl;
	std::cout << "1. Yes" << std::endl;
	std::cout << "2. No" << std::endl;
	std::cout << std::endl;
	std::cout << "Choice: ";
	std::cin >> choice;
	std::cout << std::endl;

	while (choice < 1 || choice > 2) {

		std::cout << "Invalid choice!" << std::endl;
		std::cout << std::endl;

		std::cout << "Would you like to switch the status of the grid?" << std::endl;
		std::cout << std::endl;
		std::cout << "1. Yes" << std::endl;
		std::cout << "2. No" << std::endl;
		std::cout << std::endl;
		std::cout << "Choice: ";
		std::cin >> choice;
		std::cout << std::endl;
	}

	if (choice == 1) {

		if (tech->return_safety()) {

			item_ptr =  tech->return_safety();

			if (item_ptr->get_item_name() == "High Voltage") {

				std::cout << "Switching the grid..." << std::endl;
				std::cout << std::endl;
				g->set_grid();
			}

			else {

				std::cout << tech->get_name() << " does not have their high voltage safety gear!";
				std::cout << std::endl;
				std::cout << tech->get_name() << " receives an electric shock!" << std::endl;
				std::cout << std::endl;

				tech->set_lose();
			}
		}	

		else {

			std::cout << tech->get_name() << " does not have their high voltage safety gear!";
			std::cout << std::endl;
			std::cout << tech->get_name() << " receives an electric shock!" << std::endl;
			std::cout << std::endl;

			tech->set_lose();
		}
	}
}

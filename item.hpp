/****************************************************************************************
 * * Program Filename: item.hpp
 * * Author: Corey Immke
 * * Date: 3/8/16
 * * Description: This is the specification file for the item class.
 * * Input: There is no input as this is the specification file.
 * * Output: There is no output as this is the specification file.
 ***************************************************************************************/
#ifndef ITEM_HPP
#define ITEM_HPP

#include <string>

class item {

	private:
		int type;
		std::string item_name;

	public:
		item(int, std::string);
		int get_type();
		std::string get_item_name();

};

#endif

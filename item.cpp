/****************************************************************************************
 * * Program Filename: item.cpp
 * * Author: Corey Immke
 * * Date: 3/12/16
 * * Description: This is the implementation file for the item class. This is 
 * *			  used for building parts or safety objects to be used by the player.
 * * Input: There is no input as this is the implementation file.
 * * Output: There is no output as this is the implementation file.
 ***************************************************************************************/
#include "item.hpp"

#include <string>

/****************************************************************************************
 * * Function: item(int, std::string)
 * * Description: This is the constructor for the item class. 
 * * Parameters: An int for the item type where 1 is a part and 2 is a safety object and
 * *			 a string for the item name
 * * Pre-Conditions: An item object is created.
 * * Post-Conditions: The type and item_name fields are set.
 ***************************************************************************************/
item::item(int type, std::string name) {

	this->type = type;
	item_name = name;
}

/****************************************************************************************
 * * Function: get_type()
 * * Description: This function returns an int for the item type 
 * * Parameters: None
 * * Pre-Conditions: An item object is created and the type is set.
 * * Post-Conditions: Either a 1 or 2 is returned.
 ***************************************************************************************/
int item::get_type() {

	return type;
}

/****************************************************************************************
 * * Function: get_item_name()
 * * Description: This function returns an string for the item name 
 * * Parameters: None
 * * Pre-Conditions: An item object is created and the name is set.
 * * Post-Conditions: The name of the object is returned.
 ***************************************************************************************/
std::string item::get_item_name() {

	return item_name;
}

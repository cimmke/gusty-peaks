/****************************************************************************************
 * * Program Filename: turbine.cpp
 * * Author: Corey Immke
 * * Date: 3/9/16
 * * Description: This is the implementation file for the turbine class. This is 
 * *			  one of the spaces the player can be in.
 * * Input: There is no input other than using the special_action().
 * * Output: There is no output as this is the implementation file.
 ***************************************************************************************/
#include "turbine.hpp"
#include "item.hpp"

#include <iostream>

/****************************************************************************************
 * * Function: turbine(std::string, std::string)
 * * Description: This is the default constructor for the turbine class. 
 * * Parameters: The space name and space type strings
 * * Pre-Conditions: A turbine object is created.
 * * Post-Conditions: The space_name and space_type fields are set. The turbine
 * *				  object's private data elements are set.
 ***************************************************************************************/
turbine::turbine(std::string name, std::string type) : space(name, type){

	transformer = 0;
	yaw = 0;
	weather_station = 0;
	online = 0;
}

/****************************************************************************************
 * * Function: special_action(technician*, game*)
 * * Description: This is the special action for the turbine class. It attempts to
 * *			  repair the turbine. If the turbine is offline, the tech is carrying
 * *		      the correct part, and has the correct safety gear if needed, they will
 * *			  repair the turbine. If those scenarios are not met then either the
 * *			  player gets an error message or they lose the game. 
 * * Parameters: A pointer to the technician object being used in the game and a pointer
 * *			 to the game itself
 * * Pre-Conditions: During the turn() function the player has chosen the special 
 * *				 action while the technician is located at a turbine space.
 * * Post-Conditions: Either the turbine is fixed and returned online, the turbine is
 * *				  not able to be fixed, or the player loses by not having the
 * *				  correct safety gear.
 ***************************************************************************************/
void turbine::special_action(technician *tech, game *g) {

	/*
	 * Check that the turbine is actually offline
	 */
	if (online == 1) {

		if (transformer == 1) {

			/*
			 * The transformer cannot be repaired while the grid is still active.
			 */
			if (g->get_grid() == 0) {

				std::cout << "The grid is still active. Cannot repair transformer!";
				std::cout << std::endl;
			}

			else {

				/*
				 * Verify correct part in tech_bag
				 */
				if (check_correct_part(tech, "Transformer Part")) {

					std::cout << "Transformer repaired!" << std::endl;
					std::cout << "Turbine returned online!" << std::endl;

					/*
					 * Set turbine data elements so it will show up online
					 * via the laptop. Delete the part since it was used.
					 */
					transformer = 0;
					online = 0;
					tech->delete_part();
				}

				else {

					std::cout << tech->get_name() << " doesn't have the correct part!";
					std::cout << std::endl;
					std::cout << "Turbine cannot be fixed." << std::endl;
				}
			}
		}

		/*
		 * Turbine is faulted due to the yaw system
		 */
		else if (yaw == 1) {

			/*
			 * Verify technician has climbing gear in the tech_bag
			 */
			if (check_correct_safety_gear(tech)) {

				if (check_correct_part(tech, "Yaw Part")) {

					std::cout << "Yaw System repaired!" << std::endl;
					std::cout << "Turbine returned online!" << std::endl;

					yaw = 0;
					online = 0;
					tech->delete_part();
				}

				else {

					std::cout << tech->get_name() << " doesn't have the correct part!";
					std::cout << std::endl;
					std::cout << "Turbine cannot be fixed." << std::endl;
				}
			}

			/*
			 * Climbing gear was not in the tech_bag so player loses
			 */
			else {

				std::cout << tech->get_name() << " did not have their climbing gear!";
				std::cout << std::endl;
				std::cout << tech->get_name() << " has slipped and fallen down the ";
				std::cout << "ladder!" << std::endl;

				tech->set_lose();
			}
		}

		/*
		 * Turbine is offline due to weather station
		 */
		else if (weather_station == 1) {

			if (check_correct_safety_gear(tech)) {

				if (check_correct_part(tech, "Weather Station Part")) {

					std::cout << "Weather Station repaired!" << std::endl;
					std::cout << "Turbine returned online!" << std::endl;

					weather_station = 0;
					online = 0;
					tech->delete_part();
				}

				else {

					std::cout << tech->get_name() << " doesn't have the correct part!";
					std::cout << std::endl;
					std::cout << "Turbine cannot be fixed." << std::endl;
				}
			}

			else {

				std::cout << tech->get_name() << " did not have their climbing gear!";
				std::cout << std::endl;
				std::cout << tech->get_name() << " has slipped and fallen down the ";
				std::cout << "ladder!" << std::endl;

				tech->set_lose();
			}
		}
	}

	else {

		std::cout << "The turbine is online! No action can be taken!" << std::endl;
	}

	std::cout << std::endl;

}

/****************************************************************************************
 * * Function: check_correct_safety_gear(technician*)
 * * Description: Verifies the technician has the climbing gear in the tech_bag 
 * * Parameters: A pointer to the technician object being used in the game.
 * * Pre-Conditions: The player has tried to repair a turbine
 * * Post-Conditions: If the climbing gear is in the bag then true is returned,
 * *				  otherwise false is returned.
 ***************************************************************************************/
bool turbine::check_correct_safety_gear(technician *tech) {

	item *item_ptr;

	if (tech->return_safety()) {

		item_ptr = tech->return_safety();

		if (item_ptr->get_item_name() == "Climbing") {

			return true;
		}

		else {

			return false;
		}
	}

	else {

		return false;
	}	
}

/****************************************************************************************
 * * Function: check_correct_part(technician*, std::string)
 * * Description: Verifies the technician has the correct part in the tech_bag 
 * * Parameters: A pointer to the technician object being used in the game, and the 
 * *			 string of the part name needed to repair the turbine.
 * * Pre-Conditions: The player has tried to repair a turbine
 * * Post-Conditions: If the part is in the bag then true is returned,
 * *				  otherwise false is returned.
 ***************************************************************************************/
bool turbine::check_correct_part(technician *tech, std::string part_name) {

	item *item_ptr;

	if (tech->return_part()) {

		item_ptr = tech->return_part();

		if (item_ptr->get_item_name() == part_name) {

			return true;
		}

		else {

			return false;
		}
	}

	else {

		return false;
	}	
}

/****************************************************************************************
 * * Function: check_if_online()
 * * Description: Determines if the turbine is online or offline and returns the
 * *			  applicable statement 
 * * Parameters: None
 * * Pre-Conditions: A turbine object is created.
 * * Post-Conditions: Either Online or Offline is returned
 ***************************************************************************************/
std::string turbine::check_if_online() {

	if (online == 1) {

		return "Offline";
	}

	else if (online == 0) {

		return "Online";
	}
}

/****************************************************************************************
 * * Function: set_online(int)
 * * Description: Sets the online data element. It should be 0 for online and 1 for
 * *			  offline 
 * * Parameters: The int value to set the online data element to.
 * * Pre-Conditions: A turbine object exists
 * * Post-Conditions: The online data element value is set.
 ***************************************************************************************/
void turbine::set_online(int online) {

	this->online = online;
}

/****************************************************************************************
 * * Function: get_transformer()
 * * Description: Gets the transformer data element 
 * * Parameters: None
 * * Pre-Conditions: A turbine object exists
 * * Post-Conditions: The transformer data element value is returned.
 ***************************************************************************************/
int turbine::get_transformer() {

	return transformer;
}

/****************************************************************************************
 * * Function: get_yaw()
 * * Description: Gets the yaw data element 
 * * Parameters: None
 * * Pre-Conditions: A turbine object exists
 * * Post-Conditions: The yaw data element value is returned.
 ***************************************************************************************/
int turbine::get_yaw() {

	return yaw;
}

/****************************************************************************************
 * * Function: get_weather_station()
 * * Description: Gets the weather_station data element 
 * * Parameters: None
 * * Pre-Conditions: A turbine object exists
 * * Post-Conditions: The weather_station data element value is returned.
 ***************************************************************************************/
int turbine::get_weather_station() {

	return weather_station;
}

/****************************************************************************************
 * * Function: set_transformer(int)
 * * Description: Sets the transformer data element 
 * * Parameters: The int value to set the transformer data element to.
 * * Pre-Conditions: A turbine has to change the status of its transformer data element.
 * * Post-Conditions: The transformer data element is set to value passed in.
 ***************************************************************************************/
void turbine::set_transformer(int transformer) {

	this->transformer = transformer;
}

/****************************************************************************************
 * * Function: set_yaw(int)
 * * Description: Sets the yaw data element 
 * * Parameters: The int value to set the yaw data element to.
 * * Pre-Conditions: A turbine has to change the status of its yaw data element.
 * * Post-Conditions: The yaw data element is set to value passed in.
 ***************************************************************************************/
void turbine::set_yaw(int yaw) {

	this->yaw = yaw;
}

/****************************************************************************************
 * * Function: set_weather_station(int)
 * * Description: Sets the weather_station data element 
 * * Parameters: The int value to set the weather_station data element to.
 * * Pre-Conditions: A turbine has to change the status of its weather_station data
 * *				 element.
 * * Post-Conditions: The weather_station data element is set to value passed in.
 ***************************************************************************************/
void turbine::set_weather_station(int weather_station) {

	this->weather_station = weather_station;
}

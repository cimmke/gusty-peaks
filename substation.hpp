/****************************************************************************************
 * * Program Filename: substation.hpp
 * * Author: Corey Immke
 * * Date: 3/8/16
 * * Description: This is the specification file for the substation class. It is derived 
 * *			  from the space class.
 * * Input: There is no input as this is the specification file.
 * * Output: There is no output as this is the specification file.
 ***************************************************************************************/
#ifndef SUBSTATION_HPP
#define SUBSTATION_HPP

#include "space.hpp"
#include "technician.hpp"
#include "game.hpp"

#include <string>

class game;

class substation : public space {

	public:
		substation();
		virtual void special_action(technician*, game*);
	
};

#endif

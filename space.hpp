/****************************************************************************************
 * * Program Filename: space.hpp
 * * Author: Corey Immke
 * * Date: 3/8/16
 * * Description: This is the specification file for the space class.
 * * Input: There is no input as this is the specification file.
 * * Output: There is no output as this is the specification file.
 ***************************************************************************************/
#ifndef SPACE_HPP
#define SPACE_HPP

#include <string>

#include "technician.hpp"

/*
 * Forward declarations so ojbects of these classes can be used as parameters for the
 * special_action() function.
 */
class technician;
class game;

class space {

	private:
		std::string space_name;
		std::string space_type;
		space *up;
		space *down;
		space *left;
		space *right;

	public:
		space(std::string, std::string);
		std::string get_space_name();
		std::string get_space_type();
		virtual void special_action(technician* t, game *g) = 0;
		space* get_up();
		space* get_down();
		space* get_left();
		space* get_right();
		void set_up(space*);
		void set_down(space*);
		void set_left(space*);
		void set_right(space*);
		virtual void set_transformer(int);
		virtual int get_transformer();
		virtual void set_yaw(int);
		virtual int get_yaw();
		virtual void set_weather_station(int);
		virtual int get_weather_station();
		virtual void set_shelves();
		virtual int get_type();
		virtual std::string get_item_name();
		virtual std::string check_if_online();
		virtual void set_online(int);
		virtual int add_part_to_bag();
		virtual int add_safety_to_bag();
		virtual int part_choice();
		virtual int safety_choice();
		virtual bool check_correct_safety_gear(technician*);
		virtual bool check_correct_part(technician*, std::string);

};

#endif

/****************************************************************************************
 * * Program Filename: office.hpp
 * * Author: Corey Immke
 * * Date: 3/8/16
 * * Description: This is the specification file for the office class. It is derived
 * *			  from the space class.
 * * Input: There is no input as this is the specification file.
 * * Output: There is no output as this is the specification file.
 ***************************************************************************************/
#ifndef OFFICE_HPP
#define OFFICE_HPP

#include "item.hpp"
#include "space.hpp"
#include "technician.hpp"

#include <queue>
#include <string>

class office : public space {

	private:
		std::queue<item*> parts;
		std::queue<item*> safety;

	public:
		office();
		~office();
		void set_shelves();
		virtual void special_action(technician*, game*);
		int add_part_to_bag();
		int add_safety_to_bag();
		int part_choice();
		int safety_choice();

};

#endif

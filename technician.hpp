/****************************************************************************************
 * * Program Filename: technician.hpp
 * * Author: Corey Immke
 * * Date: 3/8/16
 * * Description: This is the specification file for the technician class.
 * * Input: There is no input as this is the specification file.
 * * Output: There is no output as this is the specification file.
 ***************************************************************************************/
#ifndef TECHNICIAN_HPP
#define TECHNICIAN_HPP

#include "space.hpp"
#include "item.hpp"

#include <string>
#include <map>

class space;

class technician {

	private:
		std::string name;
		space *location;
		std::map<int, item*> tech_bag;
		int lose;

	public:
		technician();
		~technician();
		void set_name(std::string);
		std::string get_name();
		void set_location(space*);
		space* get_location();
		void pack_part(item*);
		void pack_safety(item*);
		void bag_contents();
		int get_lose();
		void set_lose();
		item* return_part();
		item* return_safety();
		void delete_part();

};

#endif

/****************************************************************************************
 * * Program Filename: technician.cpp
 * * Author: Corey Immke
 * * Date: 3/8/16
 * * Description: This is the implementation file for the technician class. It 
 * *			  contains the functions necessary for to move the technician and 
 * *			  carry parts and safety objects.
 * * Input: There is various input depending on the function being run. It can be a
 * *		string for a name or an integer to make a menu choice.
 * * Output: Varies depending on the function in the class.
 ***************************************************************************************/
#include "technician.hpp"

#include <cstdlib>
#include <iostream>

/****************************************************************************************
 * * Function: technician()
 * * Description: This is the default constructor for the technician class. It sets 
 * *			  default values for some of the private data elements.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: A technician object is created.
 * * Post-Conditions: name is set to "", location points to NULL, and lose is set to 0
 ***************************************************************************************/
technician::technician() {

	name = "";
	location = NULL;
	lose = 0;
}

/****************************************************************************************
 * * Function: ~technician()
 * * Description: This is the destructor for the technician class. It delets the items 
 * *			  remaining in the tech_bag map.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: A technician object is created.
 * * Post-Conditions: If a part and/or safety item are still in the tech_bag map, they
 * *				  are removed from the map and deleted.
 ***************************************************************************************/
technician::~technician() {

	item *item_ptr;

	if (tech_bag.count(1)) {
		
		item_ptr = tech_bag.at(1);
		tech_bag.erase(1);
		delete item_ptr;
	}

	if (tech_bag.count(2)) {
		
		item_ptr = tech_bag.at(2);
		tech_bag.erase(2);
		delete item_ptr;
	}
}


/****************************************************************************************
 * * Function: set_name()
 * * Description: This sets the name element to the paramter passed in.
 * * Parameters: A string to set the technician name field to.
 * * Pre-Conditions: The player enters the name they want to use for the technician.
 * * Post-Conditions: The name field is set to what the player chose it to be.
 ***************************************************************************************/
void technician::set_name(std::string name) {

	this->name = name;
}

/****************************************************************************************
 * * Function: get_name()
 * * Description: This returns the name element value of the object
 * * Parameters: None
 * * Pre-Conditions: The name of the technician is needed by the program.
 * * Post-Conditions: The name field is returned to where the function was called.
 ***************************************************************************************/
std::string technician::get_name() {

	return name;
}

/****************************************************************************************
 * * Function: set_location(space*)
 * * Description: Sets the location of the player to track what space they are in.
 * * Parameters: A pointer to the space object the player is located in
 * * Pre-Conditions: The technician object is created and a space object exists.
 * * Post-Conditions: The location field is set to the space the player is in.
 ***************************************************************************************/
void technician::set_location(space *loc) {

	location = loc;
}

/****************************************************************************************
 * * Function: get_location()
 * * Description: Gets the location of the player to see what space they are in.
 * * Parameters: None
 * * Pre-Conditions: The technician object is created and has a location.
 * * Post-Conditions: A pointer to the space ojbect the player is in is returned.
 ***************************************************************************************/
space* technician::get_location() {

	return location;
}

/****************************************************************************************
 * * Function: pack_part()
 * * Description: Adds the part passed in to the tech_bag using key 1. Key 1 in the map
 * *			  is for a part.
 * * Parameters: A pointer to the item part object to be added to the bag.
 * * Pre-Conditions: The user has selected to add a part to the bag.
 * * Post-Conditions: If an item is already in the bag with key 1 it is erased. Then
 * *				  the new part is put in the bag with the key of 1 to siginify it
 * *				  is a part.
 ***************************************************************************************/
void technician::pack_part(item *part) {

	if (tech_bag.count(1)) {

		tech_bag.erase(1);
	}

	tech_bag.insert(std::pair<int, item*>(1, part));
}

/****************************************************************************************
 * * Function: return_part()
 * * Description: Returns a pointer to the parts item in the tech_bag with a key of 1.
 * * Parameters: None
 * * Pre-Conditions: The user has selected to add a part to the bag.
 * * Post-Conditions: If an item is already in the bag with key 1 then it is returned.
 * *				  This is so it can be put back in the parts queue rather than
 * *				  being lost. If there are no parts items in the bag then NULL is
 * *				  returned.
 ***************************************************************************************/
item* technician::return_part() {

	if (tech_bag.count(1)) {

		return tech_bag.at(1);
	}

	else {

		return NULL;
	}
}

/****************************************************************************************
 * * Function: pack_safety()
 * * Description: Adds the safety item passed in to the tech_bag using key 2. Key 2 in 
 * *			  the map is for a safety item.
 * * Parameters: A pointer to the item safety object to be added to the bag.
 * * Pre-Conditions: The user has selected to add a safety item to the bag.
 * * Post-Conditions: If an item is already in the bag with key 2 it is erased. Then
 * *				  the new safety item is put in the bag with the key of 2 to 
 * *				  signify it is a safety item.
 ***************************************************************************************/
void technician::pack_safety(item *safety) {

	if (tech_bag.count(2)) {

		tech_bag.erase(2);
	}

	tech_bag.insert(std::pair<int, item*>(2, safety));
}

/****************************************************************************************
 * * Function: return_safety()
 * * Description: Returns a pointer to the safety item in the tech_bag with a key of 2.
 * * Parameters: None
 * * Pre-Conditions: The user has selected to add a safety item to the bag.
 * * Post-Conditions: If an item is already in the bag with key 2 then it is returned.
 * *				  This is so it can be put back in the parts queue rather than
 * *				  being lost. If there are no parts items in the bag then NULL is
 * *				  returned.
 ***************************************************************************************/
item* technician::return_safety() {

	if (tech_bag.count(2)) {

		return tech_bag.at(2);
	}

	else {

		return NULL;
	}
}

/****************************************************************************************
 * * Function: bag_contents()
 * * Description: Displays the contents of the tech_bag. If it is empty then it will
 * *		      state that.
 * * Parameters: None
 * * Pre-Conditions: The user has selected to the special_action() of an office object.
 * * Post-Conditions: Displays the contents of the tech_bag. If the bag is empty it
 * *				  will display a message stating the bag is empty.
 ***************************************************************************************/
void technician::bag_contents() {

	std::map<int, item*>::iterator it;

	if (!tech_bag.empty()) {

		std::cout << "Tech Bag contains:" << std::endl;

		/*
		 * Only display a part or safety gear item if one is actually in the tech
		 * bag. If only one of them is in there then it will only display that one.
		 */
		for (it = tech_bag.begin(); it != tech_bag.end(); it++) {

			if (it->first == 1) {

				std::cout << "Part: ";
				std::cout << it->second->get_item_name() << std::endl;
			}

			else if (it->first == 2) {

				std::cout << "Safety Gear: ";
				std::cout << it->second->get_item_name() << std::endl;
			}

		}
	}

	else {

		std::cout << "The tech bag is empty." << std::endl;
	}

	std::cout << std::endl;
}

/****************************************************************************************
 * * Function: get_lose()
 * * Description: Returns the value of the lose data element. If it is 0 the technician
 * *			  is still able to play. If it is 1 the technician has lost.
 * * Parameters: None
 * * Pre-Conditions: A technician object has been created.
 * * Post-Conditions: The value of lose is returned.
 ***************************************************************************************/
int technician::get_lose() {

	return lose;
}

/****************************************************************************************
 * * Function: set_lose()
 * * Description: Sets the value of lose to 1 signifying the player has lost the game.
 * * Parameters: None
 * * Pre-Conditions: A technician object has been created and they have lost the game.
 * * Post-Conditions: The value of lose is set to 1.
 ***************************************************************************************/
void technician::set_lose() {

	lose = 1;
}

/****************************************************************************************
 * * Function: delete_part()
 * * Description: Deletes the part currently in the tech_bag.
 * * Parameters: None
 * * Pre-Conditions: The part was used to fix a turbine so it must be removed from the
 * *			     game as it is no longer needed.
 * * Post-Conditions: The part item is deleted since it was dynamically allocated and
 * *				  is no longer needed.
 ***************************************************************************************/
void technician::delete_part() {

	item *item_ptr;

	if (tech_bag.count(1)) {

		item_ptr = return_part();
		tech_bag.erase(1);
		delete item_ptr;
	}
}

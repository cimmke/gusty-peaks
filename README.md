# gusty-peaks
CS162 Winter 2016 Final Project

This was my final project for CS162. The project is a command line game in which
the player is a technician on a wind farm and must repair all the wind turbines
before the end of the day. The goals of the assignment were to demonstrate OOP
using C++.

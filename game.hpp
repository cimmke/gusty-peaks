/****************************************************************************************
 * * Program Filename: game.hpp
 * * Author: Corey Immke
 * * Date: 3/8/16
 * * Description: This is the specification file for the game class.
 * * Input: There is no input as this is the specification file.
 * * Output: There is no output as this is the specification file.
 ***************************************************************************************/
#ifndef GAME_HPP
#define GAME_HPP

#include "space.hpp"
#include "technician.hpp"
#include "substation.hpp"

#include <string>

class space;

class game {

	private:
		int hour;
		int minutes_first_digit;
		int minutes_second_digit;
		space *array[5];
		int position;
		int grid_status;
		int storm_occurred;
		technician tech;

	public:
		game();
		~game();
		void opening_menu();
		void instructions();
		void tech_name();
		void scene_set();
		void build_game();
		void link_spaces();
		void link_office();
		void link_substation();
		void link_turbine1();
		void link_turbine2();
		void link_turbine3();
		void office_setup();
		void turn();
		int turn_menu();
		std::string space_special_action();
		int check_hour();
		int check_minutes_first_digit();
		void increment_time();
		void check_laptop();
		void laptop();
		std::string failure_message(int);
		void set_grid();
		int get_grid();
		void travel();
		void storm();
		int check_tech_status();
		bool win();
		void game_over();
		void credits();
};

#endif

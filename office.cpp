/****************************************************************************************
 * * Program Filename: office.cpp
 * * Author: Corey Immke
 * * Date: 3/9/16
 * * Description: This is the implementation file for the office class. This is one of
 * *			  the spaces the player can be in.
 * * Input: There is no input other than using the special_action().
 * * Output: There is no output as this is the implementation file.
 ***************************************************************************************/
#include "office.hpp"
#include "space.hpp"

#include <iostream>

/****************************************************************************************
 * * Function: office()
 * * Description: This is the default constructor for the office class. 
 * * Parameters: There are no parameters.
 * * Pre-Conditions: An office object is created.
 * * Post-Conditions: The space_name and space_type fields are set.
 ***************************************************************************************/
office::office() : space("Main", "Office") {
}

/****************************************************************************************
 * * Function: ~office()
 * * Description: This is the destructor for the office class. It ensures all parts
 * *			  and safety items that were allocated during the game and still are
 * *			  sitting in the parts and safety queue are deleted.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: An office object is created.
 * * Post-Conditions: The parts and safety queues are emptied and all items that were
 * *				  allocated and still in them are deleted.
 ***************************************************************************************/
office::~office() {

	item *item_ptr;

	while (!parts.empty()) {

		item_ptr = parts.front();
		parts.pop();
		delete item_ptr;
	}

	while (!safety.empty()) {

		item_ptr = safety.front();
		safety.pop();
		delete item_ptr;
	}
}

/****************************************************************************************
 * * Function: set_shelves()
 * * Description: This function creates the parts and safety gear items and sets them
 * *			  in their respective queues (sets them on the shelf). 
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The build_game() function has been called.
 * * Post-Conditions: Three parts are put in the parts queue and two safety gear items
 * *				  are put in the safety queue.
 ***************************************************************************************/
void office::set_shelves() {

	item *item_ptr;

	item_ptr = new item(1, "Transformer Part");
	parts.push(item_ptr);

	item_ptr = new item(1, "Yaw Part");
	parts.push(item_ptr);

	item_ptr = new item(1, "Weather Station Part");
	parts.push(item_ptr);

	item_ptr = new item(2, "High Voltage");
	safety.push(item_ptr);

	item_ptr = new item(2, "Climbing");
	safety.push(item_ptr);
}

/****************************************************************************************
 * * Function: special_action(technician*, game*)
 * * Description: This function is the office space special_action function. It allows
 * *			  a technician to add a part and/or safety gear item to their bag. 
 * * Parameters: A technician pointer to the technician object that can add items
 * * Pre-Conditions: A technician object is created and their location is an office.
 * *				 The parts and safety queue's must also exist.
 * * Post-Conditions: Depending on the user choice but a part and/or safety item could
 * *				  be added to the tech_bag of the technician passed in. Any items
 * *				  previously in the tech_bag will be returned to the queues since
 * *				 they can only carry one of each.
 ***************************************************************************************/
void office::special_action(technician *tech, game *g) {

	item *item_ptr;
	int part_index = 1;
	int safety_index = 1;

	tech->bag_contents();

	/*
	 * Check if user wants to add a part to the tech_bag. If they do, the user chooses
	 * the part and it is added to the bag. If another part was in the tech_bag, it
	 * is placed on the shelf.
	 */
	if (add_part_to_bag() == 1) {

		int part = part_choice();

		while (part_index != part) {

			item_ptr = parts.front();
			parts.pop();
			parts.push(item_ptr);
			part_index++;
		}

		/*
		 * Verify NULL is not returned prior to trying to push the old part into the
		 * parts queue.
		 */
		if (tech->return_part()) {

			parts.push(tech->return_part());
		}

		tech->pack_part(parts.front());
		parts.pop();	
	}

	/*
	 * Check if the user wants to add a safety item to the tech_bag. Again the user can
	 * choose which item and if they already have an item in their bag, it is returned
	 * to the shelf.
	 */
	if (add_safety_to_bag() == 1) {

		int safety_val = safety_choice();

		while (safety_index != safety_val) {

			item_ptr = safety.front();
			safety.pop();
			safety.push(item_ptr);
			safety_index++;
		}
		
		if (tech->return_safety()) {

			safety.push(tech->return_safety());
		}

		tech->pack_safety(safety.front());
		safety.pop();	
	}

	tech->bag_contents();

}

/****************************************************************************************
 * * Function: add_part_to_bag()
 * * Description: This function asks the user if they want to add a part to the bag.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The user has chosen the special_action() function in an office
 * * Post-Conditions: Either a 1 is returned if the user wishes to add a part or a 2
 * *				  is returned if they do not want to add a part.
 ***************************************************************************************/
int office::add_part_to_bag() {

	int choice;

	std::cout << "Would you like to add a new part to the bag?" << std::endl;
	std::cout << "1. Yes" << std::endl;
	std::cout << "2. No" << std::endl;
	std::cout << std::endl;
	std::cout << "Choice: ";
	std::cin >> choice;
	std::cout << std::endl;

	while (choice < 1 || choice > 2) {

		std::cout << "Invalid Choice!" << std::endl;
		std::cout << std::endl;

		std::cout << "Would you like to add a new part to the bag?" << std::endl;
		std::cout << "1. Yes" << std::endl;
		std::cout << "2. No" << std::endl;
		std::cout << std::endl;
		std::cout << "Choice: ";
		std::cin >> choice;
		std::cout << std::endl;
	}

	return choice;
}

/****************************************************************************************
 * * Function: add_safety_to_bag()
 * * Description: This function asks the user if they want to add a safety item to the 
 * *			  tech_bag
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The user has chosen the special_action() function in an office
 * * Post-Conditions: Either a 1 is returned if the user wishes to add a safety item 
 * *				  or a 2 is returned if they do not want to add a safety item.
 ***************************************************************************************/
int office::add_safety_to_bag() {

	int choice;

	std::cout << "Would you like to add new safety gear to the bag?" << std::endl;
	std::cout << "1. Yes" << std::endl;
	std::cout << "2. No" << std::endl;
	std::cout << std::endl;
	std::cout << "Choice: ";
	std::cin >> choice;
	std::cout << std::endl;

	while (choice < 1 || choice > 2) {

		std::cout << "Invalid Choice!" << std::endl;
		std::cout << std::endl;

		std::cout << "Would you like to add new safety gear to the bag?" << std::endl;
		std::cout << "1. Yes" << std::endl;
		std::cout << "2. No" << std::endl;
		std::cout << std::endl;
		std::cout << "Choice: ";
		std::cin >> choice;
		std::cout << std::endl;
	}

	return choice;
}

/****************************************************************************************
 * * Function: part_choice()
 * * Description: This function displays a list of potential parts and the user chooses
 * *			  which one to add.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The user has chosen to add a part to their tech_bag
 * * Post-Conditions: The int value for the part based on their location in the queue
 * *				  is returned with 1 being the starting location.
 ***************************************************************************************/
int office::part_choice() {

	int choice;
	item *item_ptr;

	std::cout << "Parts shelf contents:" << std::endl;

	for (int i = 0; i < parts.size(); i++) {

		std::cout << i + 1 << ". " << parts.front()->get_item_name() << std::endl;
		item_ptr = parts.front();
		parts.pop();
		parts.push(item_ptr);
	}

	std::cout << std::endl;

	std::cout << "Enter the integer value of the part you wish to add." << std::endl;
	std::cout << std::endl;
	std::cout << "Choice: ";
	std::cin >> choice;
	std::cout << std::endl;

	while (choice < 1 || choice > parts.size()) {

		std::cout << "Invalid choice!" << std::endl;
		std::cout << std::endl;
	
		std::cout << "Enter the integer value of the part you wish to add." << std::endl;
		std::cout << std::endl;
		std::cout << "Choice: ";
		std::cin >> choice;
		std::cout << std::endl;
	}

	return choice;
}

/****************************************************************************************
 * * Function: safety_choice()
 * * Description: This function displays a list of potential safety items and the user
 * *			  chooses which one to add.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The user has chosen to add a safety item to their tech_bag
 * * Post-Conditions: The int value for the safety item based on its location in the 
 * *				  queue is returned with 1 being the starting location.
 ***************************************************************************************/
int office::safety_choice() {

	int choice;
	item *item_ptr;

	std::cout << "Safety shelf contents:" << std::endl;

	/*
	 * Perform sequence of pop() and push() to cycle through each item in the queue
	 */
	for (int j = 0; j < safety.size(); j++) {

		std::cout << j + 1 << ". " <<  safety.front()->get_item_name() << std::endl;
		item_ptr = safety.front();
		safety.pop();
		safety.push(item_ptr);
	}
	
	std::cout << std::endl;

	std::cout << "Enter the integer value of the safety gear you wish to add.";
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << "Choice: ";
	std::cin >> choice;
	std::cout << std::endl;

	while (choice < 1 || choice > safety.size()) {

		std::cout << "Invalid choice!" << std::endl;
		std::cout << std::endl;
	
		std::cout << "Enter the integer value of the safety gear you wish to add.";
		std::cout << std::endl;
		std::cout << std::endl;
		std::cout << "Choice: ";
		std::cin >> choice;
		std::cout << std::endl;
	}

	return choice;
}

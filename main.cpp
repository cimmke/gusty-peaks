/****************************************************************************************
 * * Program Filename: main.cpp
 * * Author: Corey Immke
 * * Date: 3/14/16
 * * Description: This is the driver for the game. It first calls the functions to 
 * *			  create and build the game, then runs a turn of the game until either
 * *			  time expires, the player loses, or the player repairs all the turbines.
 * *			  Finally, the win or lose message is displayed based on the result.
 * * Input: There is various input depending on the function being run via the game
 * *		class. Other than the player name, everything is menu driven with interger
 * *		input used to make choices.
 * * Output: There is various output based on the direction the game goes. At the end,
 * *		 a win or lose message is displayed.
 ***************************************************************************************/
#include "game.hpp"

#include <ctime>
#include <cstdlib>
#include <iostream>

int main() {

	srand(time(0)); /* Ensure storms are randomly created */

	game wind;

	wind.opening_menu();

	wind.scene_set();

	wind.build_game();

	/*
	 * The end game time is 16:30. Therefore adding the hour and first digit of
	 * those would be 19. This makes one simple check rather than checking both.
	 */
	while ((wind.check_hour() + wind.check_minutes_first_digit() != 19)
			&& (wind.check_tech_status() == 0) && !(wind.win())) {

		wind.turn();
	}

	if (wind.win()) {

		wind.credits();
	}

	else if (wind.check_tech_status() == 1) {

		wind.game_over();
	}

	else {

		std::cout << std::endl;
		std::cout << "Time has expired!" << std::endl;
		std::cout << std::endl;
		wind.game_over();
	}


	return 0;
}

/****************************************************************************************
 * * Program Filename: turbine.hpp
 * * Author: Corey Immke
 * * Date: 3/8/16
 * * Description: This is the specification file for the turbine class. It is derived 
 * *			  from the space class.
 * * Input: There is no input as this is the specification file.
 * * Output: There is no output as this is the specification file.
 ***************************************************************************************/
#ifndef TURBINE_HPP
#define TURBINE_HPP

#include "space.hpp"
#include "technician.hpp"
#include "game.hpp"

#include <string>

class game;

class turbine : public space {

	private:
		int transformer;
		int yaw;
		int weather_station;
		int online;

	public:
		turbine(std::string, std::string);
		virtual void special_action(technician*, game*);
		bool check_correct_safety_gear(technician*);
		bool check_correct_part(technician*, std::string);
		std::string check_if_online();
		void set_online(int);
		virtual int get_transformer();
		virtual int get_yaw();
		virtual int get_weather_station();
		virtual void set_transformer(int);
		virtual void set_yaw(int);
		virtual void set_weather_station(int);

};

#endif

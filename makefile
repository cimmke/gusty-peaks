#########################################################
# File: Makefile
# Description: A more robust makefile for CS162.
# You need to edit the variables under "USER SECTION".
# To execute the makefile on unix-y systems, type 'make'.
# If that doesn't work, 'make -f makefile' might be useful.
#########################################################
 

# CXX is a standard variable name used for the compiler. 
CXX = g++

# CXXFLAGS is a standard variable name for compile flags.
# -std=c++0x specifies to use a certain language version.
CXXFLAGS = -std=c++0x

# -Wall turns on all warnings
CXXFLAGS += -Wall

# -pedantic-errors strictly enforces the standard
CXXFLAGS += -pedantic-errors

# -g turns on debug information 
CXXFLAGS += -g

####################
### USER SECTION ###
####################

# SRCS is a standard name for the source files. 
# Edit as needed.
SRC1 = space.cpp
SRC2 = office.cpp
SRC3 = substation.cpp
SRC4 = turbine.cpp
SRC5 = technician.cpp
SRC6 = item.cpp
SRC7 = game.cpp
SRC8 = main.cpp
SRCS = ${SRC1} ${SRC2} ${SRC3} ${SRC4} ${SRC5} ${SRC6} ${SRC7} ${SRC8}

# HEADERS is a standard name for the header files. 
# Edit these as needed.
HEADER1 = space.hpp
HEADER2 = office.hpp
HEADER3 = substation.hpp
HEADER4 = turbine.hpp
HEADER5 = technician.hpp
HEADER6 = item.hpp
HEADER7 = game.hpp
HEADERS = ${HEADER1} ${HEADER2} ${HEADER3} ${HEADER4} ${HEADER5} ${HEADER6} ${HEADER7}

# These will be your executable names. 
# Edit as needed.
PROG1 = wind

#####################
### BUILD SECTION ###
#####################

# Typing 'make' in terminal calls the first build availible.
# In this case, default.
default:
	${CXX} ${SRCS} -o ${PROG1}


# Typing 'make clean' calls this build.
# It's designed to clean up the folder.
# Be careful with this, edit as needed.
clean: 
	rm -f ${PROG1} *.o *~ *.out


/****************************************************************************************
 * * Program Filename: game.cpp
 * * Author: Corey Immke
 * * Date: 3/8/16
 * * Description: This is the implementation file for the game class. It contains the
 * *			  functions to help build and run the game.
 * * Input: There is various input depending on the function being run. It can be a
 * *		string for a name or an integer to make a menu choice.
 * * Output: There is various text output functions as well as output from playing the
 * *		 game.
 ***************************************************************************************/
#include "game.hpp"
#include "office.hpp"
#include "turbine.hpp"
#include "substation.hpp"
#include "space.hpp"

#include <iostream>
#include <string>
#include <iomanip>
#include <cstdlib>
#include <unistd.h>

/****************************************************************************************
 * * Function: game()
 * * Description: This is the default constructor for the game class. It sets the game
 * *			  start time and the grid to on.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: A game() object is created.
 * * Post-Conditions: The game start time is set and the grid is set to on.
 ***************************************************************************************/
game::game() {

	hour = 7;
	minutes_first_digit = 0;
	minutes_second_digit = 0;
	grid_status = 0;
	storm_occurred = 0;
}

/****************************************************************************************
 * * Function: ~game()
 * * Description: This is the destructor for the game class. It deletes all the spaces
 * *			  allocated in the array.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: A game() object is created.
 * * Post-Conditions: The game has ended.
 ***************************************************************************************/
game::~game() {

	for (int i = 0; i < 5; i++) {

		delete array[i];
	}
}

/****************************************************************************************
 * * Function: opening_menu()
 * * Description: This is opening menu for the game. It allows the user to start the
 * *			  game or choose to view the instructions.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: A game() object has been created and a game set to run.
 * * Post-Conditions: The game either starts or displays the instructions.
 ***************************************************************************************/
void game::opening_menu() {

	int choice;

	std::cout << std::endl;
	std::cout << "WELCOME TO TROUBLE AT GUSTY PEAKS WIND FARM" << std::endl;
	std::cout << std::endl;

	std::cout << "1. Start" << std::endl;
	std::cout << "2. Instructions" << std::endl;
	std::cout << std::endl;
	std::cout << "Choice: ";
	std::cin >> choice;
	std::cout << std::endl;

	/*
	 * Error check
	 */
	while (choice < 1 || choice > 2) {

		std::cout << "Invalid input. Please enter 1 or 2" << std::endl;
		std::cout << std::endl;

		std::cout << "1. Start" << std::endl;
		std::cout << "2. Instructions" << std::endl;
		std::cout << std::endl;
		std::cout << "Choice: ";
		std::cin >> choice;
		std::cout << std::endl;
	}

	if (choice == 1) {

		tech_name();
	}

	else {

		instructions();
	}

}

/****************************************************************************************
 * * Function: instructions()
 * * Description: This displays the basic instructions for the game including a few tips. 
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The player elected to view the instructions from the opening menu.
 * * Post-Conditions: The instructions are displayed and the user moves on to start the
 * *				  game.
 ***************************************************************************************/
void game::instructions() {

	std::cout << "Instructions" << std::endl;
	std::cout << std::endl;
	std::cout << "The objective of the game is to fix all three of the wind ";
	std::cout << std::endl;
	std::cout << "turbines that have faulted due to failed components. The ";
	std::cout << std::endl;
	std::cout << "game starts at 7:00 in the office with three failed ";
	std::cout << std::endl;
	std::cout << "wind turbines. The game is turn based with each turn taking ";
	std::cout << std::endl;
	std::cout << "30 minutes of clock time. Each turbine must be fixed by ";
	std::cout << std::endl;
	std::cout << "16:30 or the player loses. The player is a technician ";
	std::cout << std::endl;
	std::cout << "who has a tech bag in which they can carry one part that ";
	std::cout << std::endl;
	std::cout << "can fix specific failed systems/components and one piece ";
	std::cout << std::endl;
	std::cout << "of safety gear needed to safely fix or interact with ";
	std::cout << std::endl;
	std::cout << "compenents. Parts and safety gear are stored in the office ";
	std::cout << std::endl;
	std::cout << "and the player can only pick to carry these from there. ";
	std::cout << std::endl;
	std::cout << "Each turn consists of letting the player view the park ";
	std::cout << std::endl;
	std::cout << "status from their laptop and then either travel to a new ";
	std::cout << std::endl;
	std::cout << "location or interact with the current space they are in.";
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << "Tips:" << std::endl;
	std::cout << "1. The substation is used to turn the high voltage grid on or off.";
	std::cout << std::endl;
	std::cout << "   This step is key for fixing the transformer.";
	std::cout << std::endl;
	std::cout << "2. Be sure to check the laptop for the failure message to help";
	std::cout << std::endl;
	std::cout << "   identify which part may fix the turbine." << std::endl;
	std::cout << "3. If you do not have the correct safety gear in your tech bag ";
	std::cout << std::endl;
	std::cout << "   and try to interact with the space the player will lose.";
	std::cout << std::endl;
	std::cout << "   *Hint* The yaw and weather station require climbing.";
	std::cout << std::endl;
	std::cout << "4. Random storms can occur causing the player to lose 1 hour ";
	std::cout << std::endl;
	std::cout << "   and start back in the office." << std::endl;
	std::cout << std::endl;

	usleep(10000000);

	tech_name();
	
}

/****************************************************************************************
 * * Function: tech_name()
 * * Description: Prompts the user to enter the name of the technician and saves it in
 * *			  tech's name data element. 
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The game has started.
 * * Post-Conditions: The name data element of the tech object is set to the user input
 ***************************************************************************************/
void game::tech_name() {

	std::string name;

	std::cout << "Please enter the name of the technician: ";
	std::cin.ignore();
	std::getline(std::cin, name);
	tech.set_name(name);
	std::cout << std::endl;
}

/****************************************************************************************
 * * Function: scene_set()
 * * Description: Displays the opening scene text. 
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The game has started and the player has entered the tech's name.
 * * Post-Conditions: The opening scene text is displayed to the user.
 ***************************************************************************************/
void game::scene_set() {

	std::cout << "You are a wind technician at the Gusty Peaks Wind Farm. " << std::endl;
	std::cout << "You arrive at work promptly at 7:00 to find all three ";
	std::cout << std::endl;
	std::cout << "of the wind turbines are offline and not producing power ";
	std::cout << std::endl;
	std::cout << "due to some failed component. The weather forecast calls ";
	std::cout << std::endl;
	std::cout << "for high winds starting at 16:30 later that day. You " << std::endl;
	std::cout << "must get all three turbines back online by 16:30 so " << std::endl;
	std::cout << "that the wind farm can capitalize on the winds and produce ";
	std::cout << std::endl;
	std::cout << "its maximum power. You must do so by having the correct part ";
	std::cout << std::endl;
	std::cout << "with you. Remember safety first so make sure to have the ";
	std::cout << std::endl;
	std::cout << "correct safety equipment with you and watch out for stray ";
	std::cout << std::endl;
	std::cout << "thunderstorms!" << std::endl;
	std::cout << std::endl;

	/*
	 * Allow the player to read the screen before jumping in.
	 */
	usleep(10000000);

}

/****************************************************************************************
 * * Function: build_game()
 * * Description: Creates the spaces, sets them up to their default state, and puts the
 * *			  technician in the office. 
 * * Parameters: There are no parameters.
 * * Pre-Conditions: A game object has been created and the user has elected to play
 * * Post-Conditions: The default configuration for the game is constructed and set.
 ***************************************************************************************/
void game::build_game() {

	space* space_ptr;
	
	space_ptr = new office();
	array[0] = space_ptr;

	space_ptr = new substation();
	array[1] = space_ptr;

	space_ptr = new turbine("#1", "Turbine");
	space_ptr->set_transformer(1);
	space_ptr->set_online(1);
	array[2] = space_ptr;

	space_ptr = new turbine("#2", "Turbine");
	space_ptr->set_yaw(1);
	space_ptr->set_online(1);
	array[3] = space_ptr;

	space_ptr = new turbine("#3", "Turbine");
	space_ptr->set_weather_station(1);
	space_ptr->set_online(1);
	array[4] = space_ptr;

	link_spaces();

	office_setup();

	tech.set_location(array[0]); /* Place the technician in the office to start */
}

/****************************************************************************************
 * * Function: link_spaces()
 * * Description: Runs through each space and performs their function to link their
 * *			  up, down, left, and right pointers 
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The build_game() function has been called and the spaces are all set
 * * Post-Conditions: Each space has it's pointers linked to another space.
 ***************************************************************************************/
void game::link_spaces() {

	link_office();
	link_substation();
	link_turbine1();
	link_turbine2();
	link_turbine3();
}

/****************************************************************************************
 * * Function: link_office()
 * * Description: Sets the up, down, left, and right pointers for the office object 
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The build_game() function has been called and the office space is
 * *				 stored in array[0].
 * * Post-Conditions: Each space has it's pointers linked to another space.
 ***************************************************************************************/
void game::link_office() {

	array[0]->set_up(array[1]); /* Up set to substation */
	array[0]->set_down(array[3]); /* Down set to 2 Turbine */
	array[0]->set_left(array[2]); /* Left set to 1 Turbine */
	array[0]->set_right(array[4]); /* Right set to 3 Turbine */
}

/****************************************************************************************
 * * Function: link_substation()
 * * Description: Sets the up, down, left, and right pointers for the substation object 
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The build_game() function has been called and the substation space
 * *				 is stored in array[1].
 * * Post-Conditions: Each space has it's pointers linked to another space.
 ***************************************************************************************/
void game::link_substation() {

	array[1]->set_up(array[3]); /* Up set to 2 Turbine */
	array[1]->set_down(array[0]); /* Down set to Office */
	array[1]->set_left(array[2]); /* Left set to 1 Turbine */
	array[1]->set_right(array[4]); /* Right set to 3 Turbine */
}

/****************************************************************************************
 * * Function: link_turbine1()
 * * Description: Sets the up, down, left, and right pointers for the turbine object 
 * *			  with the name 1.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The build_game() function has been called and the turbine 1 space is
 * *				 stored in array[2].
 * * Post-Conditions: Each space has it's pointers linked to another space.
 ***************************************************************************************/
void game::link_turbine1() {

	array[2]->set_up(array[1]); /* Up set to substation */
	array[2]->set_down(array[3]); /* Down set to 2 Turbine */
	array[2]->set_left(array[4]); /* Left set to 3 Turbine */
	array[2]->set_right(array[0]); /* Right set to Office */
}

/****************************************************************************************
 * * Function: link_turbine2()
 * * Description: Sets the up, down, left, and right pointers for the turbine object 
 * *			  with the name 2.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The build_game() function has been called and the turbine 2 space is
 * *				 stored in array[3].
 * * Post-Conditions: Each space has it's pointers linked to another space.
 ***************************************************************************************/
void game::link_turbine2() {

	array[3]->set_up(array[0]); /* Up set to office */
	array[3]->set_down(array[1]); /* Down set to substation */
	array[3]->set_left(array[2]); /* Left set to 1 Turbine */
	array[3]->set_right(array[4]); /* Right set to 3 Turbine */
}

/****************************************************************************************
 * * Function: link_turbine3()
 * * Description: Sets the up, down, left, and right pointers for the turbine object 
 * *			  with the name 3.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The build_game() function has been called and the turbine 3 space is
 * *				 stored in array[4].
 * * Post-Conditions: Each space has it's pointers linked to another space.
 ***************************************************************************************/
void game::link_turbine3() {

	array[4]->set_up(array[1]); /* Up set to substation */
	array[4]->set_down(array[3]); /* Down set to 2 Turbine */
	array[4]->set_left(array[0]); /* Left set to Office */
	array[4]->set_right(array[2]); /* Right set to 1 Turbine */
}

/****************************************************************************************
 * * Function: office_setup()
 * * Description: Sets the office up so it has the parts shelf and safety shelf in their
 * *			  default state.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The build_game() function has been called and the office space is
 * *				 stored in array[0].
 * * Post-Conditions: The parts and safety shelves of the office are set.
 ***************************************************************************************/
void game::office_setup() {

	array[0]->set_shelves();
}

/****************************************************************************************
 * * Function: turn()
 * * Description: Drives one turn of the game. Consists of randomly generating storms,
 * *			  letting the user choose to view the laptop or not, performing either
 * *			  the special action for the room or traveling to another space, and
 * *			  finally incrementing the game time by 30 minutes.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The build_game() function has been run. The game time has not
 * *			     expired and the player has neither won or lost via some means.
 * * Post-Conditions: The game time has incremented by 30 minutes, and the player has
 * *				  either successfully interacted with the space, lost due to their
 * *				  interaction with the space, or traveled to a new location.
 ***************************************************************************************/
void game::turn() {
	
	/*
	 * Storms can only occur once per game. Only try to generate a storm if one has not
	 * occurred yet.
	 */
	if (storm_occurred == 0) {

		storm();
	}

	check_laptop();

	int action = turn_menu();

	/*
	 * Either perform space action or travel based on user choice.
	 */
	if (action == 1) {
	
		tech.get_location()->special_action(&tech, this);
	}

	else if (action == 2) {

		travel();
	
		std::cout << tech.get_name() << " is now at "; 
		std::cout << tech.get_location()->get_space_name();
		std::cout << " " << tech.get_location()->get_space_type() << std::endl;
		std::cout << std::endl;
	}

	increment_time();
}

/****************************************************************************************
 * * Function: turn_menu()
 * * Description: Provides a menu for asking the user if they want to perform an action
 * *			  or travel. Special action text is depending on the technician
 * *			  location.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The turn() function has been called.
 * * Post-Conditions: The int value of the player's choice is returned to the turn()
 * *				  function in order to be used.
 ***************************************************************************************/
int game::turn_menu() {

	int choice;

	std::cout << "Do you wish to perform an action or travel?";
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << "1. " << space_special_action() << std::endl;
	std::cout << "2. Travel" << std::endl;
	std::cout << std::endl;
	std::cout << "Choice: ";
	std::cin >> choice;
	std::cout << std::endl;

	while (choice < 1 || choice > 2) {

		std::cout << "Invalid choice!" << std::endl;
		std::cout << std::endl;

		std::cout << "Do you wish to perform an action or travel?";
		std::cout << std::endl;
		std::cout << std::endl;
		std::cout << "1. " << space_special_action() << std::endl;
		std::cout << "2. Travel" << std::endl;
		std::cout << std::endl;
		std::cout << "Choice: ";
		std::cin >> choice;
		std::cout << std::endl;
	}
	
	return choice;
}

/****************************************************************************************
 * * Function: space_special_action()
 * * Description: Each space has a unique special action. This checks the technicians
 * *			  location and returns the special action phrase based on where they
 * *			  are located.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The turn() function has been called.
 * * Post-Conditions: The location specific special action phrase is returned to the
 * *				  turn_menu() function.
 ***************************************************************************************/
std::string game::space_special_action() {

	space *current = tech.get_location();

	if (current->get_space_type() == "Office") {

		return "Select items from parts and safety shelves";
	}

	else if (current->get_space_type() == "Substation") {

		return "Turn grid on/off";
	}

	else if (current->get_space_type() == "Turbine") {

		if (current->check_if_online() == "Online") {

			return "Turbine is online. No action available";
		}

		else {

			return "Repair turbine.";
		}
	}
}

/****************************************************************************************
 * * Function: check_hour()
 * * Description: Returns the value of the data element hour.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: A game object exists.
 * * Post-Conditions: The value of the data element hour is returned.
 ***************************************************************************************/
int game::check_hour() {

	return hour;
}

/****************************************************************************************
 * * Function: check_minutes_first_digit()
 * * Description: Returns the value of the data element minutes_first_digit.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: A game object exists.
 * * Post-Conditions: The value of the data element minutes_first_digit is returned.
 ***************************************************************************************/
int game::check_minutes_first_digit() {

	return minutes_first_digit;
}

/****************************************************************************************
 * * Function: increment_time()
 * * Description: Increments the current game time by 30 minutes. The game uses military
 * *			  time.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The turn() function has been called.
 * * Post-Conditions: The game time is incremented by 30 minutes.
 ***************************************************************************************/
void game::increment_time() {

	/*
	 * If the current time is at the half hour
	 */
	if (minutes_first_digit == 3) {

		hour++;
		minutes_first_digit = 0;
	}

	/*
	 * If the current time is at the top of the hour.
	 */
	else {

		minutes_first_digit = 3;
	}
}

/****************************************************************************************
 * * Function: check_laptop()
 * * Description: Checks if the user wants to run the laptop. If they do then it runs
 * *			  the laptop() function otherwise it does nothing.
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The build_game() function has been run so the turbines are set up.
 * * Post-Conditions: Either the laptop() function is run or nothing occurs.
 ***************************************************************************************/
void game::check_laptop() {

	int choice;

	std::cout << "Do you wish to view the laptop to check the time and turbine status?";
	std::cout << std::endl;
	std::cout << std::endl;
	std::cout << "1. Yes" << std::endl;
	std::cout << "2. No" << std::endl;
	std::cout << std::endl;
	std::cout << "Choice: ";
	std::cin >> choice;
	std::cout << std::endl;

	while (choice < 1 || choice > 2) {

		std::cout << "Invalid choice!" << std::endl;
		std::cout << std::endl;

		std::cout << "Do you wish to view the laptop to check the time and turbine status?";
		std::cout << std::endl;
		std::cout << std::endl;
		std::cout << "1. Yes" << std::endl;
		std::cout << "2. No" << std::endl;
		std::cout << std::endl;
		std::cout << "Choice: ";
		std::cin >> choice;
		std::cout << std::endl;
	}

	if (choice == 1) {

		laptop();
	}
}

/****************************************************************************************
 * * Function: laptop()
 * * Description: Displays the laptop screen which shows the user the current game time
 * *			  and the status of each of the wind turbines
 * * Parameters: There are no parameters.
 * * Pre-Conditions: The build_game() function has been run so the turbines are set up.
 * * Post-Conditions: The game time and turbine status are displayed
 ***************************************************************************************/
void game::laptop() {

	std::cout << "Time: " << hour << ":" << minutes_first_digit;
	std::cout << minutes_second_digit << std::endl;
	std::cout << std::endl;
 
	std::cout << "Turbine  " << std::setw(9) << std::left << "Status";
	std::cout << "Failure Message" << std::endl;

	for (int i = 2; i < 5; i++) {

		std::cout << "   " << array[i]->get_space_name() << "    ";

		/*
		 * Ensures turbine says offline if no grid, even if turbine is fixed
		 */
		if (get_grid() == 0) {

			std::cout << std::setw(9) << array[i]->check_if_online();
		}

		else {

			std::cout << std::setw(9) << "Offline";
		}

		std::cout << failure_message(i);
		std::cout << std::endl;
	}

	std::cout << std::endl;

}

/****************************************************************************************
 * * Function: failure_message()
 * * Description: Returns the appropriate failure message for the turbines so it can
 * *			  be used by the laptop() function.
 * * Parameters: There is an int parameter to signify the location in the array of
 * *			 space pointers we are interested in.
 * * Pre-Conditions: The build_game() function has been run and the array set up with
 * *				 the turbines at subscripts 2, 3, and 4.
 * * Post-Conditions: The failure message for the particular turbine is returned so
 * *				  laptop() can print it in the correct format.
 ***************************************************************************************/
std::string game::failure_message(int i) {

	/*
	 * Check transformer first. This way if there is no grid, it still shows the fault
	 * message to help the player better know which turbine had the transformer issue.
	 */
	if (array[i]->get_transformer() == 1) {

		return "High Voltage Transformer Not Functioning";
	}
	
	else if (get_grid() == 1) {

			return "No Grid";
	}
		
	else if (array[i]->check_if_online() == "Online") {

		return " ";
	}

	else {

		if (array[i]->get_yaw() == 1) {

			return "Yaw System Not Functioning";
		}

		else if (array[i]->get_weather_station() == 1) {

			return "Weather Station Not Functioning";
		}
	}
}

/****************************************************************************************
 * * Function: get_grid()
 * * Description: Returns the status of the grid. 0 means the grid is on, 1 means it is
 * *			  turned off
 * * Parameters: None
 * * Pre-Conditions: The grid_status data element has been set prior
 * * Post-Conditions: The int value for grid_status is returned.
 ***************************************************************************************/
int game::get_grid() {

	return grid_status;
}

/****************************************************************************************
 * * Function: set_grid()
 * * Description: Sets the status of the grid. 0 means the grid is on, 1 means it is
 * *			  turned off. It will set the grid to the opposite of what it was
 * *			  previously.
 * * Parameters: None
 * * Pre-Conditions: The grid_status data element has been set prior
 * * Post-Conditions: The grid_status element is swapped to either 1 or 0 based on its
 * *				  previous value.
 ***************************************************************************************/
void game::set_grid() {

	if (grid_status == 0) {

		grid_status = 1;
	}

	else {

		grid_status = 0;
	}
}

/****************************************************************************************
 * * Function: travel()
 * * Description: This function gives the menu of locations the player can travel to,
 * *			  lets them make the choice, and then moves the player to that location.
 * * Parameters: None
 * * Pre-Conditions: The technician is currently located in a space and turn() as been
 * *				 called.
 * * Post-Conditions: The technician's location data element is set to the new location
 * *				  the user chose to move to.
 ***************************************************************************************/
void game::travel() {

	space *current = tech.get_location();
	int choice;

	std::cout << "Please choose where you want to travel to." << std::endl;
	std::cout << "1. " << current->get_up()->get_space_name() << " ";
	std::cout << current->get_up()->get_space_type() << std::endl;
	
	std::cout << "2. " << current->get_down()->get_space_name() << " ";
	std::cout << current->get_down()->get_space_type() << std::endl;

	std::cout << "3. " << current->get_left()->get_space_name() << " ";
	std::cout << current->get_left()->get_space_type() << std::endl;

	std::cout << "4. " << current->get_right()->get_space_name() << " ";
	std::cout << current->get_right()->get_space_type() << std::endl;

	std::cout << std::endl;
	std::cout << "Choice: ";
	std::cin >> choice;
	std::cout << std::endl;

	while (choice < 1 || choice > 4) {

		std::cout << "Inalid Choice!" << std::endl;
		std::cout << std::endl;

		std::cout << "Please choose where you want to travel to." << std::endl;
		std::cout << "1. " << current->get_up()->get_space_name() << " ";
		std::cout << current->get_up()->get_space_type() << std::endl;
	
		std::cout << "2. " << current->get_down()->get_space_name() << " ";
		std::cout << current->get_down()->get_space_type() << std::endl;

		std::cout << "3. " << current->get_left()->get_space_name() << " ";
		std::cout << current->get_left()->get_space_type() << std::endl;

		std::cout << "4. " << current->get_right()->get_space_name() << " ";
		std::cout << current->get_right()->get_space_type() << std::endl;

		std::cout << std::endl;
		std::cout << "Choice: ";
		std::cin >> choice;
		std::cout << std::endl;
	}

	if (choice == 1) {

		tech.set_location(current->get_up());
	}

	if (choice == 2) {

		tech.set_location(current->get_down());
	}

	if (choice == 3) {

		tech.set_location(current->get_left());
	}

	if (choice == 4) {

		tech.set_location(current->get_right());
	}
}

/****************************************************************************************
 * * Function: storm()
 * * Description: Randomly generates a storm. A storm has a 20% chance of occurring and
 * *			  can only occur once during the game. If a storm occurs, the player is
 * *			  moved to the office and time is incremented by an hour until the 
 * *			  storm passes.
 * * Parameters: None
 * * Pre-Conditions: A storm has never occurred before
 * * Post-Conditions: If a storm is generated then the player is moved to the office
 * *				  and hour is incremented by 1. Also an output message is generated
 * *				  to let the player know what happened. If the storm is not
 * *				  generated then nothing happens.
 ***************************************************************************************/
void game::storm() {

	int random = rand() % 100 + 1;

	if (random <= 20) {

		tech.set_location(array[0]); /* Place the technician in the office */
		hour++;

		std::cout << "CRACK!" << std::endl;
		std::cout << "Lightning has struck near the wind farm." << std::endl;
		std::cout << tech.get_name() << " has returned to the office to ";
		std::cout << "wait out the storm." << std::endl;
		std::cout << "One hour passes until the weather has cleared." << std::endl;
		std::cout << std::endl;

		storm_occurred = 1;

		usleep(5000000);
	}
}

/****************************************************************************************
 * * Function: win()
 * * Description: Checks if all the turbines are online. If they are then true is 
 * *			  returned the game is over since the player won.
 * * Parameters: None
 * * Pre-Conditions: The game object exists and the player has not yet lost.
 * * Post-Conditions: If all three turbines are online the true is returned, otherwise
 * *				  false is returned.
 ***************************************************************************************/
bool game::win() {
	
	int count = 0;

	for (int i = 2; i < 5; i++) {

		if (array[i]->check_if_online() == "Offline") {

			count++;
		}
	}

	if (count > 0) {

		return false;
	}

	else if (grid_status == 1) {

		return false;
	}

	else {

		return true;
	}
}

/****************************************************************************************
 * * Function: check_tech_status()
 * * Description: Returns the lose data element of the technician object which specifies
 * *			  if the player has lost or not.
 * * Parameters: None
 * * Pre-Conditions: The game is still ongoing and the technician object exists.
 * * Post-Conditions: 0 is returned if the technician is still playing, 1 is returned
 * *				  if the technician has lost.
 ***************************************************************************************/
int game::check_tech_status() {

	return tech.get_lose();
}

/****************************************************************************************
 * * Function: game_over()
 * * Description: Displays the message GAME OVER!
 * * Parameters: None
 * * Pre-Conditions: The player has lost the game.
 * * Post-Conditions: The game is over and the program terminates.
 ***************************************************************************************/
void game::game_over() {

	std::cout << "GAME OVER!" << std::endl;
	std::cout << std::endl;
}

/****************************************************************************************
 * * Function: credits()
 * * Description: Displays the winning message 
 * * Parameters: None
 * * Pre-Conditions: The player has won the game by fixing all three turbines.
 * * Post-Conditions: The game is over and the program terminates.
 ***************************************************************************************/
void game::credits() {

	std::cout << "CONGRATULATIONS! YOU FIXED ALL THE TURBINES!" << std::endl;
	std::cout << std::endl;
}

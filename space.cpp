/****************************************************************************************
 * * Program Filename: space.cpp
 * * Author: Corey Immke
 * * Date: 3/9/16
 * * Description: This is the implementation file for the space class. This is the base
 * *			  class for the spaces the player can be in.
 * * Input: There is no input as this is an abstract object.
 * * Output: There is no output as this is the implementation file.
 ***************************************************************************************/
#include "space.hpp"

class technician;

/****************************************************************************************
 * * Function: space(std::string, std::string)
 * * Description: This is the default constructor for the space class. 
 * * Parameters: Two strings one for the space_name and one for the space_type.
 * * Pre-Conditions: A space object is created.
 * * Post-Conditions: The space_name and space_type fields are set.
 ***************************************************************************************/
space::space(std::string space_name, std::string space_type) {

	this->space_name = space_name;
	this->space_type = space_type;
}

/****************************************************************************************
 * * Function: get_space_name()
 * * Description: This function returns the name of the space. 
 * * Parameters: None
 * * Pre-Conditions: A space object is created and the name has been set.
 * * Post-Conditions: The space_name field is returned.
 ***************************************************************************************/
std::string space::get_space_name() {

	return space_name;
}

/****************************************************************************************
 * * Function: get_space_type()
 * * Description: This function returns the type of the space. 
 * * Parameters: None
 * * Pre-Conditions: A space object is created and the type has been set.
 * * Post-Conditions: The space_type field is returned.
 ***************************************************************************************/
std::string space::get_space_type() {

	return space_type;
}

/****************************************************************************************
 * * Function: get_up()
 * * Description: This function returns a pointer to the space that is linked via the up
 * *			  pointer 
 * * Parameters: None
 * * Pre-Conditions: A space object is created and has a space pointer set in up
 * * Post-Conditions: The space pointer stored in up is returned.
 ***************************************************************************************/
space* space::get_up() {

	return up;
}

/****************************************************************************************
 * * Function: get_down()
 * * Description: This function returns a pointer to the space that is linked via the 
 * *			  down pointer 
 * * Parameters: None
 * * Pre-Conditions: A space object is created and has a space pointer set in down
 * * Post-Conditions: The space pointer stored in down is returned.
 ***************************************************************************************/
space* space::get_down() {

	return down;
}

/****************************************************************************************
 * * Function: get_left()
 * * Description: This function returns a pointer to the space that is linked via the 
 * *			  left pointer 
 * * Parameters: None
 * * Pre-Conditions: A space object is created and has a space pointer set in left
 * * Post-Conditions: The space pointer stored in left is returned.
 ***************************************************************************************/
space* space::get_left() {

	return left;
}

/****************************************************************************************
 * * Function: get_right()
 * * Description: This function returns a pointer to the space that is linked via the 
 * *			 right pointer 
 * * Parameters: None
 * * Pre-Conditions: A space object is created and has a space pointer set in right
 * * Post-Conditions: The space pointer stored in right is returned.
 ***************************************************************************************/
space* space::get_right() {

	return right;
}

/****************************************************************************************
 * * Function: set_up(space*)
 * * Description: This function sets the space up pointer to link the space to another 
 * * Parameters: A space* to the space to link via the up pointer
 * * Pre-Conditions: At least two spaces are created that can be linked.
 * * Post-Conditions: The up pointer is set to point to another space object.
 ***************************************************************************************/
void space::set_up(space* u) {

	up = u;
}

/****************************************************************************************
 * * Function: set_down(space*)
 * * Description: This function sets the space down pointer to link the space to another 
 * * Parameters: A space* to the space to link via the down pointer
 * * Pre-Conditions: At least two spaces are created that can be linked.
 * * Post-Conditions: The down pointer is set to point to another space object.
 ***************************************************************************************/
void space::set_down(space* d) {

	down = d;
}

/****************************************************************************************
 * * Function: set_left(space*)
 * * Description: This function sets the space left pointer to link the space to another 
 * * Parameters: A space* to the space to link via the left pointer
 * * Pre-Conditions: At least two spaces are created that can be linked.
 * * Post-Conditions: The left pointer is set to point to another space object.
 ***************************************************************************************/
void space::set_left(space *l) {

	left = l;
}

/****************************************************************************************
 * * Function: set_right(space*)
 * * Description: Sets the space right pointer to link the space to another 
 * * Parameters: A space* to the space to link via the right pointer
 * * Pre-Conditions: At least two spaces are created that can be linked.
 * * Post-Conditions: The right pointer is set to point to another space object.
 ***************************************************************************************/
void space::set_right(space *r) {

	right = r;
}

/*
 * The remainder of the functions are virtual functions that aren't used by the space
 * class but defined so the derived class that needs this function can have its
 * particular version of the function run.
 */

/****************************************************************************************
 * * Function: set_transformer(int)
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A turbine object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
void space::set_transformer(int t) {
}

/****************************************************************************************
 * * Function: get_transformer()
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A turbine object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
int space::get_transformer() {
}

/****************************************************************************************
 * * Function: set_yaw(int)
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A turbine object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
void space::set_yaw(int y) {
}

/****************************************************************************************
 * * Function: get_yaw()
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A turbine object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
int space::get_yaw() {
}

/****************************************************************************************
 * * Function: set_weather_station(int)
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A turbine object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
void space::set_weather_station(int w) {
}

/****************************************************************************************
 * * Function: get_weather_station()
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A turbine object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
int space::get_weather_station() {
}

/****************************************************************************************
 * * Function: set_shelves()
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: An office object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
void space::set_shelves() {
}

/****************************************************************************************
 * * Function: get_type()
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: An office object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
int space::get_type() {
}

/****************************************************************************************
 * * Function: get_item_name()
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: An office object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
std::string space::get_item_name() {
}

/****************************************************************************************
 * * Function: check_if_online()
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A turbine object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
std::string space::check_if_online() {
}

/****************************************************************************************
 * * Function: set_online(int)
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A turbine object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
void space::set_online(int o) {
}

/****************************************************************************************
 * * Function: add_part_to_bag()
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A techncian object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
int space::add_part_to_bag() {
}

/****************************************************************************************
 * * Function: add_safety_to_bag()
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A technician object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
int space::add_safety_to_bag() {
}

/****************************************************************************************
 * * Function: part_choice()
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A technician object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
int space::part_choice() {
}

/****************************************************************************************
 * * Function: safety_choice()
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A technician object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
int space::safety_choice() {
}

/****************************************************************************************
 * * Function: check_correct_safety_gear()
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A technician object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
bool space::check_correct_safety_gear(technician *t) {
}

/****************************************************************************************
 * * Function: check_correct_part()
 * * Description: This function does nothing. 
 * * Parameters: None
 * * Pre-Conditions: A technician object has been created.
 * * Post-Conditions: None
 ***************************************************************************************/
bool space::check_correct_part(technician *t, std::string name) {
}
